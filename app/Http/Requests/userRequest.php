<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class userRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return  [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password'=>'required|confirmed',
            'phone'=>'unique:users|required|numeric',
            'address'=>'required|string|max:100000000'
        ];


    }
        public function messages(){
            return $messages=    [
                'name.required'=> 'name-of-user required',
                'name.string'=> 'name-of-user.string',
                'name.max'=> 'name-of-user.max',

                'email.required'=> 'email.required',
                'email.string'=> 'email.string',
                'email.email'=> 'email.has to be email',
                'email.max'=> 'email.max',
                'email.unique'=> 'email.unique',
                'password.required'=> 'password.required',
                'password.confirmed'=> 'password.confirmed',
                'phone.required'=> 'phone.required',
                'phone.unique'=> 'phone.unique',
                'phone.numeric'=> 'phone.numeric',
                'address.max'=> 'address.max',
                'address.required'=> 'address.required',
                'address.string'=> 'address.string',




            ];
        }
}
