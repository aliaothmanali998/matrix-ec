<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class productRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return  [
            'name' => 'required|unique:products|max:255',
            'category_id' => 'required',
            'description'=>'required|max:1024',
            'photo'=>'required',
            'productnumber'=>'unique:products|required|numeric|max:100000',
            'price'=>'required|numeric|max:100000000'
        ];
    }
        public function messages(){
            return $messages=    [
                'name.required'=> 'name-of-product required',
                'name.unique'=> 'name-of-product.unique',
                'name.max'=> 'name-of-product.max',
               'category_id.required'=> 'category.required',
                'description.required'=> 'description.required',
                'description.max'=> 'description.max',
                'price.required'=> 'price.required',
                'price.numeric'=> 'price.numeric',
                'price.max'=> 'price.max',
                'productnumber.required'=>'product-number.required',
                'productnumber.numeric'=> 'product-number.numeric',
                'productnumber.max'=> 'product-number.max',
                'photo.required'=> 'photo.required',



            ];
        }
}
