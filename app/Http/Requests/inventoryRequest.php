<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class inventoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return  [

            'product_id' => 'required',
            'type'=>'required',
            'qty'=>'required|numeric|max:100000',

        ];
    }
        public function messages(){
            return $messages=    [

               'product_id.required'=> 'product name.required',
                'type.required'=> 'type.required',
                'qty.required'=> 'quantity.required',
                'qty.numeric'=>'quantity.numeric',
                'qty.max'=>'quantity.max',




            ];
        }
}
