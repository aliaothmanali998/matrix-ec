<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Traits\ProductTrait;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Product;
use App\Models\Inventory;
use App\Models\Category;
use App\Models\Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     */





    public function showProWebDetails($id){
        $product = Product::find($id);
        $catnav=Category::get();
        return view ('front.pages.product.product-details', compact(['product', 'catnav']));
    }

    public function addToCart(Request $request,$id){

        if(Auth::id()) {//make sure that this user login
            $user=Auth::user();
          $product=Product::find($id);
            $quantity=$product->quantity;
            if($product->quantity - $request->quantity >= 0){
                $cart=Cart::create([
//                    'name'=>$user->name,
//                    'email'=>$user->email,
                    'user_id'=>$user->id,
//                    'product'=>$product->name,
                    'price'=>$product->price * $request->quantity,
//                    'photo'=>$product->photo,
                    'product_id'=>$id,
                    'quantity'=>$request->quantity


                ]);
                return redirect()->back();
            }
            else{
                return redirect()->back()->with('quantity','you have to chose amount less than quantity of product');
            }


        }
        else{
            return redirect('login');
        }
    }




    public function showCart(){
        if(Auth::id()){
            $id=Auth::user()->id;
            $carts=Cart::where('user_id',$id)->get();
            $catnav=Category::get();
            return view ('front.pages.cart.showcart',compact(['carts', 'catnav']));
        }
        else{
            return redirect('login');
        }

    }


    public function stripe(){
        return view('home.strip',compact('totalprice'));
    }


    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy (string $id)
    {
        Cart::where('id', $id)->delete();
        return redirect('cart')
            ->with('deletemessage','the product has been deleted successfully');
    }

}
