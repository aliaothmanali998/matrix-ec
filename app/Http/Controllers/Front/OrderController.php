<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Traits\ProductTrait;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Product;
use App\Models\Inventory;
use App\Models\Category;
use App\Models\Cart;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     */

    public function cashOrder(){

        $user=Auth::user();
        $userid=$user->id;
        $datac=Cart::where('user_id', $userid)->get();
   //     dd($data);

        foreach ($datac as $data)
        {
            $order=Order::create([
//                'name'=>$data->name,
//                'email'=>$data->email,
                'user_id'=>$data->user_id,
//                'product'=>$data->product,
                'price'=>$data->price * $data->quantity,
                'photo'=>$data->photo,
                'product_id'=>$data->product_id,
                'quantity'=>$data->quantity,
                'payment_status' =>'cash on delivery',
                'delivery_status' => 'processing',
            ]);

            $product_iid=$data->product_id;

            $product=Product::find($product_iid);
//            dd($product);
            $qty= $product->quantity;

            $qty -= $data->quantity;
            Inventory::create([
                'qty' =>$data->quantity,
                'type' => 1,
                'product_id' =>$product_iid
            ]);



            Product::where('id', $product_iid)->update([

                'quantity' => $qty ,
            ]);








            $cart_id=$data->id;
            $cart=Cart::find($cart_id);
            $cart->delete();

//            Inventory::where('name',$name);

        }
        return redirect('cart')->with('message', 'We have received your order. we will connect you soon...');

    }



    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
