<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\userRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $users = User::get();
        return view('admin.pages.users.mange-user', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        Return view('admin.pages.users.createuser');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(userRequest $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:'.User::class],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

//        $user = User::create([
//            'name' => $request->name,
//            'email' => $request->email,
//            'phone'=> $request->phone,
//            'password' => Hash::make($request->password),
//        ]);
        return redirect('/admin/user')->with(['success' => 'تم إضافة المستخدم بنجاح']);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $user = User::find($id);
        return view ('admin.pages.users.user-details', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        return view('admin.pages.users.edituser')
        ->with('user',User::where('id',$id)->first());
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        User::where('id',$id)
        ->update([  'name' => $request->name,
        'email' => $request->email,
        'password' => Hash::make($request->password),]);
        return redirect('/admin/user')->with(['success' => 'تم تعديل بيانات المستخدم بنجاح']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        User::where('id', $id)->delete();
        return redirect('/admin/user')
        ->with('deletemessage', 'تم حذف المستخدم بنجاح');
    }
}
