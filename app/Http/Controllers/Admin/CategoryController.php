<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\Inventory;
use Illuminate\Http\Request;

class CategoryController extends Controller
{




    public function index()
    {    $categories = Category::get();

         Return view('admin.pages.category.category', compact('categories'));


    }


    public function create()
    {

    Return view('admin.pages.category.createcategory');
    }


    public function store(Request $request)
    {

        $request->validate([
            'name' => ['required', 'string', 'max:255'],

        ]);
        $input = $request->all();


        Category::create($input);
        return redirect('/admin/category')->with(['success' => 'تم إضافة الفئة بنجاح']);
    }


    public function show(string $id)
    {
        $categories = Category::find($id);

        Return view('admin.pages.category.relatedproduct', compact('categories'));
    }


    public function edit(string $id)
    {
        return view('admin.pages.category.editcategory')
        ->with('category',Category::where('id',$id)->first());
    }

    public function update(Request $request, string $id)
    {

        $request->validate([
            'name' => ['required', 'string', 'max:255'],

        ]);
        Category::where('id',$id)
        ->update([ 'name' => $request->input('name')]);

        return redirect('/admin/category')->with(['success' => 'تم تعديل الفئة بنجاح']);
    }


    public function destroy(string $id)
    {
        Category::where('id', $id)->delete();
        return redirect('/admin/category')
        ->with('deletemessage', 'تم حذف الفئة بنجاح');
    }


    public function showProduct($category_id){

        $categories = Category::find($category_id);

        return view('admin.pages.category.relatedproduct', compact('categories'));



    }


        public function  delete($id)
        {
            Product::where('id', $id)->delete();
            $categories = Category::get();
            return view('admin.pages.category.relatedproduct', compact('categories'))
            ->with('deletemessage', 'تم حذف المنتج بنجاح');

    }


    public function destroyRelation(Request $request){

$id=$request->id;
$category_id=$request->category_id;

$categories = Category::find($request ->category_id);


$categories -> products () -> detach ($id); //بتعمل update  للعلاقة في جدول ال pivot       اي اذا بدي اعمل تعديل على العلاقة بين منتج و كاتيجوري



return redirect(route('admin.showproduct', $category_id))->with(['success' => 'تم حذف العلاقة بنجاح']);




    }



    public function relatedCat($id){

        $products = Product::select('id', 'name') -> get();
        $allcategories =  Category::where('id', $id)->get();

        return view ('admin.pages.category.category-with-product', compact('products', 'allcategories'));
    }

    public function storeRelated(Request $request){


        $category_id=$request ->category_id;
        $category = Category::find($request ->category_id);
         if(!$category)
         return abort ('404');

         $category -> products () -> syncWithoutDetaching ($request->product_id);  // رح تعمل اضافة لعلاقات جديدة مع الحفاظ على القديم بدون تكرار


return redirect(route('admin.showproduct', $category_id))->with(['successp' => 'تم ربط المنتجات بنجاح']);


}


}


// return Category::with('products')->find(1);

//سوف تستخدم لاحقا
        //return view('admin. pages.category.showrelatedproduct',compact('products'));



         // return view('admin.pages.category.relatedproduct')
        // ->with('deletecatmessage', 'تم حذف العلاقة بنجاح');


         //$product -> categories () -> attach ($request->category_id);       تسمح  بتكرار العلاقة في ال pivot table
         //$product -> categories () -> sync ($request->category_id);  بتعمل update  للعلاقة في جدول ال pivot       اي اذا بدي اعمل تعديل على العلاقة بين منتج و كاتيجوري
