<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\productRequest;
use App\Http\Requests\updateProducRequest;
use App\Models\Category;
use App\Models\Product;
use App\Models\Inventory;
use App\Traits\ProductTrait;

class ProductController extends Controller
{

    use ProductTrait;
    /**
     * Display a listing of the resource.
     */
    public function index()

    {
        $products = Product::get();
        return view('admin.pages.product.product', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()

    {
        $categories = Category::get();
        return view ('admin.pages.product.createproduct', compact('categories') );

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(productRequest $request)
    {

//dd($request->all());


       $file_name = $this -> savePhoto($request->photo , 'images/products');

        Product::query()->create(
            [
                'photo' => $file_name,
                'name' => $request->input('name'),
                'productnumber' => $request->input('productnumber'),
                'description' => $request->input('description'),

                'price' => $request->input('price')

            ]
        );
       $product= Product::orderby('created_at', 'desc')->first();


        $product -> categories () -> attach ($request->category_id);

        return redirect(route('admin.product'))->with(['success' => 'تم إضافة المنتج بنجاح']);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $product = Product::find($id);
        return view ('admin.pages.product.product-details', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {  $product=Product::find($id)->get();
        return view('admin.pages.product.editproduct', compact('product'))
        ->with('product',Product::where('id',$id)->first());
    }
    /**
     * Update the specified resource in storage.
     */
    public function update(updateProducRequest $request)
    {
//        $input = $request->all();

        $file_name = $this -> savePhoto($request -> photo , 'images/products');



        Product::where('id',$request->id)

        ->update(
            [

                    'photo' => $file_name,
                    'name' => $request->input('name'),
                    'productnumber' => $request->input('productnumber'),
                    'description' => $request->input('description'),

                    'price' => $request->input('price')


            ]
        );

        return redirect(route('admin.product'))->with(['success' => 'تم إضافة المنتج بنجاح']);
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {  Inventory::where('product_id',$id)->delete();
        Product::where('id', $id)->delete();
        return redirect('/admin/product')
        ->with('deletemessage', 'تم حذف المنتج بنجاح');

    }



    public function related ($id){



        $products = Product::where('id', $id) -> get();
        $allcategories = Category::select('id', 'name') -> get();
        return view ('admin.pages.product.category-with-product', compact('products', 'allcategories'));
    }

    public function storeRelated(Request $request){



       $product = Product::find($request ->product_id);
        if(!$product)
        return abort ('404');
        //$product -> categories () -> attach ($request->category_id);       تسمح  بتكرار العلاقة في ال pivot table
        //$product -> categories () -> sync ($request->category_id);  بتعمل update  للعلاقة في جدول ال pivot       اي اذا بدي اعمل تعديل على العلاقة بين منتج و كاتيجوري
        $product -> categories () -> syncWithoutDetaching ($request->category_id);  // رح تعمل اضافة لعلاقات جديدة مع الحفاظ على القديم بدون تكرار

        return redirect(route('admin.product'))->with(['successp' => 'تم ربط المنتج بنجاح']);
    }
}










      //  $categories = Category::select('id','name') ->get(); كنت اعرض الكاتيغوري الموجودة بالداتا بيس ممكن ارجع استخدمها


       // $input['category'] = json_encode($input['category']);//طريقة لتخزين مصفوفة'   category' => $request->input('category'),

       // $input['category'] = json_encode($input['category']);


        //Product::create($input);
       // $product = Product::find($request ->id);
        //if(!$product)//  اذا بدي اعمل لاحقا ميثود لربط منتجات موجود فعليا عندي بالنظام
        //return abort('404');
       // $product ->categories()-> attach ($request -> category);    //many to many insert to data base
       // Product::create($input);



       //$product = Product::find($id);
        //$categories = $product -> categories;//في حال كان عندو كاتيجوري سابقا


        //redirect->back()->with(['message' => 'هذا المنتج غير موجود')


        // $products = Product::latest()->first();
      // $products = Product::query()
      // ->latest()
      // ->first()->id;
       // dd($products);
       // $products = Product::table('products')->latest('upload_time')->first();
