<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Traits\ProductTrait;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Product;
use App\Models\Inventory;
use App\Models\Category;
use App\Models\Cart;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
       // $orders=Order::get()
        $myCollection=Order::orderby('created_at', 'desc')->get();

        $uniqueCollection = $myCollection->unique(['user_id']);




     // $uniqueCollection->all();


   //     dd($uniqueCollection);

//        $allorders=Order::get();
//
//        $orders=Order::where('email',$allorders->email)->where('created_at',$allorders->created_at)->get(first);
        return view('admin.pages.orders.orders', compact('uniqueCollection'));

    }
    public function delivered(Request $request){

        $order=Order::find($request->created_at);
        $prices=Order::where('created_at',$request->created_at)->update([
            'delivery_status'=> 'Delivered',
        ]);

//        $order->delivery_status="delivered";
//        $order->save;
        return redirect()->back();
    }



    public function showOrderdetails( Request $request){

        $id="$request->user_id";
        $created_at="$request->created_at";
        $myCollection=Order::orderby('created_at', 'desc')->where('user_id',$request->id)->get();
     //   Order::where('created_at',$request->created_at)->get();
//        $packages=$myCollection->where(['created_at']);

        $orders = $myCollection->unique(['created_at']);
//        $price=0;
//            foreach($packages as $package){
//                $price += $package->price;
//
//            }

    // $orders=Order::orderby('created_at', 'desc')->where('user_id',$id)->get();


    // dd($orders);
     return view('admin.pages.orders.orderspackge',compact(['orders']));
    }


    public function showAllOrders($id){
        $orders=Order::orderby('created_at', 'desc')->where('created_at',$id)->get();
        return view('admin.pages.orders.allorderspackge',compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}



//
//
//
//public function deliveredOne($id){
//    $order=Order::find($id);
//    $prices=Order::where('id',$id)->update([
//        'delivery_status'=> 'Delivered',
//    ]);
//
////        $order->delivery_status="delivered";
////        $order->save;
//    return redirect()->back();
//}
