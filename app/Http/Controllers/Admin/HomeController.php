<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Traits\ProductTrait;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Product;
use App\Models\Inventory;
use App\Models\Category;
use App\Models\Cart;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{

    public function redirect(){
        $usertype=Auth::user()->user_type;

        if($usertype=='1') {
            return redirect('admin/dashboard');
        }
        elseif($usertype=='0')
        {

            return redirect('/home');
        }

        else{
           redirect()->back();
        }


    }


    public function dash(){
        $productq=DB::table('products')->sum('quantity');
        $productn=DB::table('products')->count('id');
        $inventories= Inventory::whereDay('updated_at', now()->day)->get();
        $users= User::whereDay('updated_at', now()->day)->get();
        $orders= Order::whereDay('updated_at', now()->day)->get();
        //orderby('updated_at', 'desc')->first();
        return view('admin.dashboard', compact('productq', 'productn', 'inventories', 'users', 'orders'));
    }

    public function home(){
        $catnav=Category::get();
//        $productq=DB::table('products')->sum('quantity');
//        $productn=DB::table('products')->count('id');
//        $inventories= Inventory::whereDay('updated_at', now()->day)->get();
//        $users= User::whereDay('updated_at', now()->day)->get();
//        //orderby('updated_at', 'desc')->first();
//        $categories=Category::get();
        $newestproduct= Product::orderBy('created_at','desc')->take(6)->get();
        return view('front.home', compact(['newestproduct', 'catnav']));
    }




    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $newestproduct= Product::orderBy('created_at','desc')->take(6)->get();

        return view('welcome', compact('newestproduct'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $catnav=Category::get();
        $category=Category::find($id);
        return view('front.pages.pro-cat', compact(['category','catnav']));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
