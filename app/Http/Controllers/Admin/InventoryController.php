<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\inventoryRequest;
use App\Models\Category;
use App\Models\Product;
use App\Models\Inventory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $inventories = Inventory::orderby('created_at', 'desc')->get();
        return view('admin.pages.inventory.inventory', compact('inventories'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $products = Product::get();
        return view ('admin.pages.inventory.createinventory', compact('products') );
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(inventoryRequest $request)
    {
      //  dd($request);
        // $request->validate([
        //     'qty' => ['required|numeric|max:100000'],
        //     'type' => ['required'],

            // ]);
    $id=$request->product_id;
    $type= $request->type;
    $product=Product::find($request->product_id);
//dd($product);
    $quantity= $product->quantity ;
    //;
    if ($type==0){
        $quantity+=$request->qty;
    }



    elseif($type==1){

        $quantity-=$request->qty;


    }

    if($quantity<0){

        return  redirect()->back()->with('message',['you do not have this amount to ship' ]);
    }





    Product::where('id',$id)->update([

        'quantity' =>  $quantity,
    ]);



        $input = $request->all();

//        $request->validate([
//            'product_id' => 'required',
//            'qty' => 'required|numeric|max:100000',
//            'type' => 'required',
//        ]);

       Inventory::create([
        'qty' =>$request->qty,
        'type' =>$request->type,
        'product_id' =>$request->product_id
       ]);
        return redirect('/admin/inventory')->with(['success' => ' تم التعديل على الكمية بنجاح بسجل جديد']);
    }

    /**,
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        return view('admin.pages.inventory.editinventory')
        ->with('inventory',Inventory::where('id',$id)->first());

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(inventoryRequest $request, string $id)
    {
        $oldinventory=Inventory::find($id);
        $product=Product::where('id',$oldinventory->product_id)->first();
//        dd($oldinventory->product_id);
        $quantity=$product->quantity;

        $oldquantity= $oldinventory->qty;
        $oldtype=$oldinventory->type;
        $type= $request->type;
        if ($type==0  ){
            if ($request->qty >=$oldquantity && $oldtype==0){
                $quantity+=$request->qty-$oldquantity;
            }
            elseif ($oldtype==1){
                $quantity+= $request-> qty + $oldquantity;
            }
            elseif ($request-> qty < $oldquantity && $oldtype==0){
                $quantity-=   $oldquantity - $request-> qty;
            }

        }



        elseif($type==1){

            if ( $oldtype==0){
                $quantity-=  $request-> qty + $oldquantity;
            }
//            ifelse ($request->qty < $oldquantity && $oldtype==0){
//            $quantity-= $request-> qty + $oldquantity;
//
//            }
           else if ($request->qty >= $oldquantity && $oldtype==1){
            $quantity-=  $request-> qty - $oldquantity;

                                }
            else{

                $quantity+=  $oldquantity - $request-> qty;
            }

        }

        if($quantity<0){

         return  redirect()->back()->with('message',['you do not have this amount to ship' ]);
       }
        Product::where('id',$oldinventory->product_id)->update([

            'quantity' =>  $quantity,
        ]);

//        $request->validate([
//            'qty' => 'required|numeric|max:100000',
//            'type' => 'required',
//
//        ]);

        Inventory::where('id',$id)->update([
            'qty' => $request->qty,
            'type' => $request->type,
        ]);

        return redirect('/admin/inventory')->with(['success' => 'تم تعديل الفئة بنجاح']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
