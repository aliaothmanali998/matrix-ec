<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $fillable = [
         'price', 'quantity',  'product_id', 'user_id', 'payment_status',  'delivery_status'
    ];
    //'name', 'email','photo', 'product',
    public function user(){ //property
        return $this->belongsTo(User::class);
    }
    public function product(){ //property
        return $this->belongsTo(Product::class);
    }
}
