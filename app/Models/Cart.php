<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;
    protected $fillable = [
         'price', 'quantity', 'product_id', 'user_id'
    ];
    //'name', 'email',


    public function user(){ //property
        return $this->belongsTo(User::class);
    }
    public function product(){ //property
        return $this->belongsTo(Product::class);
    }

}
