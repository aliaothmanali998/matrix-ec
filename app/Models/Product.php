<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Product extends Model
{
    use HasFactory;


    protected $guarded = [];

    protected $fillable = ['name', 'photo', 'quantity', 'productnumber', 'description', 'price'];


    public function categories ():BelongsToMany
    {
        return $this->belongsToMany(Category::class, 'category_product');
    }
    public function inventory(){
        return $this->hasMany(Inventory::class);
                                }
    public function cart(){
        return $this->hasMany(Cart::class);

                           }
    public function order(){
        return $this->hasMany(Order::class);


                            }

}
