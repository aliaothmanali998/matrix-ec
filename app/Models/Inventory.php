<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    use HasFactory;
    protected $fillable = [
        'qty','type','product_id'
    ];

    public function product(){ //property
        return $this->belongsTo(Product::class);
            }
}
