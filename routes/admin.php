<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\InventoryController;
use App\Http\Controllers\Admin\OrderController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


//=======authentication=======
Route::get('/dashboard',[HomeController::class, 'dash'])->middleware(['auth', 'verified','CheckAdmin'])->name('admin.dashboard');

Route::get('/redirect',[HomeController::class,'redirect']);

//Route::get('/redirect/',[HomeController::class,'redirect']);

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';






//Route::get('/home', [App\Http\Controllers\Controller::class,'dashboard'])->name('dashboard');
Route::middleware(['CheckAdmin'])->group(function () {
    Route::name('admin.')->group(function () {


        Route::get('/inventory', function () {
            return view('admin.pages.inventory.inventory');
        })->name('inventory');


//createproduct==========================


        Route::get('/product', [ProductController::class, 'index'])->name('product');


        Route::get('/createproduct', [ProductController::class, 'create'])->name('createproduct');
        Route::post('/storeproduct', [ProductController::class, 'store'])->name('storeproduct');
//Route::get('/show1',[ProductController::class, 'index1'])->name('show1');
        Route::get('/related/{id}', [ProductController::class, 'related'])->name('makerelated');
        Route::post('/storerelated', [ProductController::class, 'storeRelated'])->name('storerelated');
        Route::get('show', [ProductController::class, 'show'])->name('showproductdetails');
        Route::get('/editproduct/{id}', [ProductController::class, 'edit'])->name('editproduct');
        Route::post('/updateproduct/{id}', [ProductController::class, 'update'])->name('updateproduct');
        Route::get('/deleteproduct/{id}', [ProductController::class, 'destroy'])->name('deleteproduct');
        Route::get('/showdetailsproduct/{id}', [ProductController::class, 'show'])->name('showdetailsproduct');


        //====================category==========
        Route::get('/createcategory', [CategoryController::class, 'create'])->name('createcategory');
        Route::post('/storecategory', [CategoryController::class, 'store'])->name('storecategory');
        Route::get('/category', [CategoryController::class, 'index'])->name('category');
        Route::get('/showproduct/{category_id}', [CategoryController::class, 'showProduct'])->name('showproduct');
        Route::get('/deleteproductcat/{id}', [CategoryController::class, 'delete'])->name('deleteproductcat');

        Route::get('/editcategory/{id}', [CategoryController::class, 'edit'])->name('editcategory');
        Route::post('/updatecategory/{id}', [CategoryController::class, 'update'])->name('updatecategory');
        Route::get('/deletecategory/{id}', [CategoryController::class, 'destroy'])->name('deletecategory');
        Route::get('/deleterelation/{id}', [CategoryController::class, 'destroyRelation'])->name('deleterelation');
        Route::get('/relatedcat/{id}', [CategoryController::class, 'relatedCat'])->name('makerelatedcat');
        Route::post('/storerelatedcat', [CategoryController::class, 'storeRelated'])->name('storerelatedcat');
        //===========================User====================
        Route::get('/createuser', [UserController::class, 'create'])->name('createuser');
        Route::post('/storeuser', [UserController::class, 'store'])->name('storeuser');
        Route::get('/user', [UserController::class, 'index'])->name('user');

        Route::get('/edituser/{id}', [UserController::class, 'edit'])->name('edituser');
        Route::post('/updateuser/{id}', [UserController::class, 'update'])->name('updateuser');
        Route::get('/deleteuser/{id}', [UserController::class, 'destroy'])->name('deleteuser');
        Route::get('/showuser/{id}', [UserController::class, 'show'])->name('showuser');

//================================Inventory=========================


        Route::get('/creatinv', [InventoryController::class, 'create'])->name('createinv');
        Route::post('/storeinv', [InventoryController::class, 'store'])->name('storeinv');
        Route::get('/inventory', [InventoryController::class, 'index'])->name('inventory');

        Route::get('/editinv/{id}', [InventoryController::class, 'edit'])->name('editinv');
        Route::post('/updateinv/{id}', [InventoryController::class, 'update'])->name('updateinv');


//=================Orders========
        Route::get('/orders', [OrderController::class, 'index'])->name('orders');
        Route::get('/delivered/{created_at}', [OrderController::class, 'delivered'])->name('delivered');
//        Route::get('/deliveredone/{id}', [OrderController::class, 'deliveredOne'])->name('deliveredone');
        Route::get('/showorderdetails/{id}', [OrderController::class, 'showOrderdetails'])->name('showorderdetails');
        Route::get('/showallorders/{name}', [OrderController::class, 'showAllOrders'])->name('showallorders');
    });
});
