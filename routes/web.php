<?php

use App\Http\Controllers\Front\CartController;
use App\Http\Controllers\Front\OrderController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'index']);



Route::get('/redirect',[HomeController::class,'redirect']);
Route::get('/home', [HomeController::class, 'home'])->name('home');
Route::get('/show/{id}', [HomeController::class, 'show'])->name('showpro');


//=========================Cart====

Route::get('/deletcat/{id}', [CartController::class, 'destroy'])->name('deletcat');
Route::get('/showprowebdetails/{id}',[CartController::class, 'showProWebDetails'])->name('showprowebdetails');
Route::post('/addtocart/{id}',[CartController::class, 'addToCart'])->name('addtocart');
Route::get('/cart', [CartController::class, 'showCart'])->name('cart');


Route::get('/cashorder', [OrderController::class, 'cashOrder'])->name('cashorder');

