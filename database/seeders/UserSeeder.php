<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::query()->create(
            ['name'=>'ali',
                'email'=>'testuser@gmail.com',
                'password'=>bcrypt('password'),
                'phone'=>'0999444333',
                'address'=>'tartous'
            ]
        );

    }
}
