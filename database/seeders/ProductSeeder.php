<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Product;
use Illuminate\Support\Facades\Hash;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
       Product::query()->create(
            [
                'id' => '1',
                'quantity'=>'100',
                'name'=>'not 10 pro',
                'photo'=>'xiaome phone.jfif',
                'productnumber'=> '11',
                'description'=>'seeder',
                'price'=>'300',
                ]
        );

    }
}
