
@extends('front.layout.app')
@section('content')




    @if(Session::has('quantity'))
        <div class="alert alert-danger" role="alert">
            <h5> {{Session::get('quantity')}} </h5>
        </div>
    @endif

    <!-- product section -->
    <section class="product_section layout_padding">
        <div class="container">


            <div class="heading_container heading_center">
                <h2>
                    Our <span>products</span>
                </h2>
            </div>

            <div class="row">






                    <div class="col-sm-6 col-md-4 col-lg-4">

                        <div class="box">

                            <div class="option_container">
                                <div class="options">
                                    <a href="{{route('showprowebdetails', 1)}}" class="option1">
                                        Product Details
                                    </a>
                                    <form  action="{{route('addtocart',1)}}" method="POST" enctype="multipart/form-data">
                                        @csrf

                                        <div class="row">

                                            <div class="col-md-4">
                                                <input type="number" name="quantity" value="1"  id="quantity"  min="1" >
                                            </div>
                                            <div>
                                                <input type="submit" name="Addtocart" value="Add To Cart"  id="" >
                                            </div>

                                        </div>


                                        {{--                                            <label for="formGroupExampleInput">name of product</label>--}}

                                    </form>

                                </div>
                            </div>
                            <div class="img-box">
                                <img src="/admin/assets/img/xiaome phone.jfif" alt="">
                            </div>


                            <h6>
                                $ 300

                            </h6>
                            <h6>
                                the total amount of this product is: 100

                            </h6>
                        </div>
                    </div>









                @foreach($newestproduct as $product)
                    @if($product->id==1)
                        @continue
                    @endif

                    <div class="col-sm-6 col-md-4 col-lg-4">

                        <div class="box">

                            <div class="option_container">
                                <div class="options">
                                    <a href="{{route('showprowebdetails', $product->id)}}" class="option1">
                                        Product Details
                                    </a>
                                    <form  action="{{route('addtocart',$product->id)}}" method="POST" enctype="multipart/form-data">
                                        @csrf

                                            <div class="row">

                                                <div class="col-md-4">
                                                    <input type="number" name="quantity" value="1"  id="quantity"  min="1" >
                                                </div>
                                              <div>
                                                  <input type="submit" name="Addtocart" value="Add To Cart"  id="" >
                                              </div>

                                            </div>


                                            {{--                                            <label for="formGroupExampleInput">name of product</label>--}}

                                    </form>

                                </div>
                            </div>
                            <div class="img-box">
                                <img src="/images/products/{{ $product->photo}}" alt="">
                            </div>

                            <h5>
                                @foreach ($product->categories as $category )
                                    <span></span>{{ $category->name }}..</span>
                                @endforeach
                            </h5>
                            <h6>
                                ${{$product->price}}

                            </h6>
                            <h6>
                               the total amount of this product is: {{$product->quantity}}

                            </h6>
                        </div>
                    </div>
                @endforeach
            </div>



        </div>
    </section>
    <!-- end product section -->




@endsection
