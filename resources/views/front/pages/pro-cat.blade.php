
@extends('front.layout.app')
@section('content')





    <!-- product section -->
    <section class="product_section layout_padding">
        <div class="container">


            <div class="heading_container heading_center">
                <h2>
                    Our <span>products</span>
                </h2>
            </div>

            <div class="row">

                @if(isset($category) && $category -> count() > 0)




                        @foreach ($category->products as $product )
                    <div class="col-sm-6 col-md-4 col-lg-4">

                        <div class="box">

                            <div class="option_container">
                                <div class="options">
                                    <a href="" class="option1">
                                        Add To Cart
                                    </a>
                                    <a href="" class="option2">
                                        Buy Now
                                    </a>
                                </div>
                            </div>
                            <div class="img-box">
                                <img src="/images/products/{{ $product->photo}}" alt="">
                            </div>

                            <h5>

                                    <span></span>{{ $category->name }}..</span>

                            </h5>
                            <h6>
                                ${{$product->price}}
                            </h6>
                        </div>
                    </div>
                @endforeach

            </div>



        </div>
        @endif
    </section>
    <!-- end product section -->




@endsection
