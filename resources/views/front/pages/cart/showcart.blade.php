@extends('front.layout.app')
@section('content')


    @if(Session::has('deletemessage'))
        <div class="alert alert-danger" role="alert">
            <h5> {{Session::get('deletemessage')}}</h5>
        </div>
    @endif
    @if(Session::has('message'))
        <div class="alert alert-danger" role="alert">
            <h5> {{Session::get('message')}}</h5>
        </div>
    @endif

    <table class="table align-content-center" id="myusertable">
        <thead>
        <tr>
            <th scope="col"> your name</th>
            <th scope="col"> your email </th>
            <th scope="col"> your phone </th>
            <th scope="col"> your address </th>
            <th scope="col"> prudoct name</th>
            <th scope="col"> prudoct photo</th>
            <th scope="col"> prudoct id</th>

            <th scope="col">price</th>
            <th scope="col">prudoct quantity</th>
            <th scope="col">operation</th>



        </tr>
        </thead>
        <tbody>


    <?php $totalprice=0; ?>


        @foreach($carts as $cart)
            <tr>
                <td>{{$cart->user->name}}</td>
                <td>{{$cart->user->email}}</td>
                <td>{{$cart->user->phone}}</td>
                <td>{{$cart->user->address}}</td>
               <td>{{$cart->product->name}}</td>
                @if($cart->product->id==1)
                    <td><img  src="/admin/assets/img/xiaome phone.jfif" style="width: 70px ; high: 50px"></td>
                @else
                <td><img  src="/images/products/{{$cart->product->photo}}" style="width: 70px ; high: 50px"></td>
                @endif
                <td>{{$cart->product_id}}</td>
                <td>${{$cart->price}}</td>
                <td scope="col">{{$cart -> quantity}}</td>


                <td> <div >
{{--                        <a href="{{ Route('admin.makerelated',$product->id) }}" class="btn btn-success p-2  ">add and edit relation</a>--}}

{{--                        <a href="{{ Route('admin.editproduct', $product->id) }}" class="btn btn-primary p-2  ">edite</a>--}}

                        <a onclick="return confirm('Are you sure to remove this product? ')" href="{{ Route('deletcat', $cart->id) }}" class="btn btn-danger p-2  ">delete product</a>
{{--                        <a href="{{ Route('admin.showdetailsproduct', $product->id) }}" class="btn btn-warning p-2  ">show details</a>--}}

                    </div>
                </td>
            </tr>
            <?php $totalprice=$totalprice + $cart->price ?>

        @endforeach
        </tbody>


    </table>
    <h4 class="text-center"> Total price is: {{$totalprice}} $ </h4>
    <div>
        <h2 class="text-center">
             <a href="{{route('cashorder')}}" class="btn btn-primary text-center ">Cash on Delivery</a>
        </h2>



    </div>

@endsection
