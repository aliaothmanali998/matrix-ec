@extends('front.layout.app')
@section('content')



<div class="container">


    <div class=" dev1 ">

        <h1 class="text-6xl font-bold pt-10">
            {{$product->name}}
        </h1>

        <span>

        <div> last update on: <span class="text-gray-500 italic"> {{date('m-d-Y'), strtotime($product->updated_at)}}</span></div>
    </div>
    <div class=" dev1 m-2 ">
        <p> has relation with these categories:</p>

        @foreach ($product->categories as $category )
            <span></span><h6>{{ $category->name }}.</h6></span>
        @endforeach</div>


    <h6>
        ${{$product->price}}
    </h6>

    <div class="container m-auto pt-15 pb-5">

        <div class="dev1">
            <div class="col-sm-6 col-md-4 col-lg-4">

                <div class="box">

                    <div class="option_container">
            <form  action="{{route('addtocart',$product->id)}} " method="POST" enctype="multipart/form-data">
                @csrf




                {{--                                            <label for="formGroupExampleInput">name of product</label>--}}
                <div class="row">

                    <div class="col-md-4">  <input type="number" name="quantity" value="1"  id="quantity"  min="1">
                    </div>
                    <div>
                        <input type="submit" name="Addtocart" value="Add To Cart"  id="" >
                    </div>

                </div>
            </form>
                        @if($product->id==1)
                            <img src="/admin/assets/img/xiaome phone.jfif" alt="">
                        @else
            <img  src="/images/products/{{ $product->photo}}" class="img-thumbnail image1 dev1 center" alt="Max-width 40% Max-high 40%" height: 600px>
        @endif
                        
                    </div >
                </div></div></div>

        <div class="div1 m-2 b-2">
<span class="dev1">
    {{$product->description}}
</span>
        </div>
@endsection
