@extends('admin.layout.app')
@section('content')


@if(Session::has('success'))
<div class="alert alert-success" role="alert">
    <h5>{{Session::get('success')}}</h5>
</div>
@endif
@if(Session::has('deletemessage'))
<div class="alert alert-danger" role="alert">
  <h5> {{Session::get('deletemessage')}}</h5>
</div>
@endif
@if(Session::has('successp'))
<div class="alert alert-success" role="alert">
    <h5>{{Session::get('successp')}}</h5>
</div>
@endif

<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">








        <div class="container-fluid py-4">


            <div class="helen">
                <a href="{{ Route('admin.createproduct') }}" class="btn btn-success p-2  ">create</a>
            </div>


            <table class="table">
                <thead>
                <tr>

                    <th scope="col">Category of prudoct</th>
                    <th scope="col">photo</th>
                    <th scope="col">Name of Product</th>

                    <th scope="col">operation</th>



                </tr>
                </thead>
                <tbody>

                        @foreach($products as $product)
                        <tr>
                            <td scope="col">
                                @foreach ($product->categories as $category )
                                    <span></span>{{ $category->name }}..</span>
                                @endforeach
                                </td>
                            <td scope="col"><img class="object-cover" src="/images/products/{{ $product -> photo}}" alt="" style="width: 100px"></td>
                            <td scope="col">{{$product -> name}}</td>


                            <td> <div >
                                    <a href="{{ Route('admin.makerelated',$product->id) }}" class="btn btn-success p-2  ">add and edit relation</a>

                                    <a href="{{ Route('admin.editproduct', $product->id) }}" class="btn btn-primary p-2  ">edite</a>

                                    <a href="{{ Route('admin.deleteproduct', $product->id) }}" class="btn btn-danger p-2  ">delete</a>
                                    <a href="{{ Route('admin.showdetailsproduct', $product->id) }}" class="btn btn-warning p-2  ">show details</a>

                            </div>
                        </td>
                        </tr>
                            @endforeach
                        </tbody>


            </table>

    </main>










@endsection


