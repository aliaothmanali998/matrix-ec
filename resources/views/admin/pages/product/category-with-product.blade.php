@extends('admin.layout.app')
@section('content')







<div class="container">

    <div class="row x">

        <div class=" col-12">
            <br>
            <br>
            <br>
            <form  action="{{ route('admin.storerelated') }}" method="POST" enctype="multipart/form-data">
                @csrf


                <div class="form-group">
                    <label for="formGroupExampleInput">chose the name of product</label>

                    <select name="product_id" class=" form-control   selectpicker" multiple="multiple" data-live-search="true" required>

                        @if(isset($products) && $products -> count() > 0)
                        @foreach($products as $product)


                        <option value="{{ $product->id }}">{{ $product->name }}</option>
                        @endforeach
                        @endif
                    </select>
                    @error('name')
                    <small class="'form-text text-danger">{{$message}}</small>
                    @enderror
                </div>


                    <div class="form-group">
                        <label for="formGroupExampleInput2">category</label>
                        <select name="category_id[]" class=" form-control   selectpicker" multiple="multiple" data-live-search="true" required>
                            @if(isset($allcategories) && $allcategories -> count() > 0)
                            @foreach($allcategories as $allcategory)


                            <option value="{{ $allcategory->id }}">{{ $allcategory->name }}</option>
                            @endforeach
                            @endif
                        </select>

                        @error('category')
                        <small class="'form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>

                            <button type="submit" class="btn btn-primary p-2 m-2">Submit</button>
                        </div>

                    </div>

            </form>
        </div>

        <div class="col-sm">

        </div>
    </div>

</div>



@endsection


