@extends('admin.layout.app')
@section('content')







<div class="container">

    <div class="row x">

        <div class=" col-12">
            <br>
            <br>
            <br>
            <form  action="{{ route('admin.storeproduct') }}" method="POST" enctype="multipart/form-data">
                @csrf


                <div class="form-group">
                    <label for="formGroupExampleInput">name of product</label>
                    <input type="text" name="name"  id="name-of-product" placeholder="name of product">
                    @error('name')
                    <small class="'form-text text-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="formGroupExampleInput">photo of product</label>
                    <input type="file" name="photo"  id="Offer Name" placeholder="photo of product">
                    @error('photo')
                    <small class="'form-text text-danger">{{$message}}</small>
                    @enderror

                    <div class="form-group">
                        <label for="formGroupExampleInput2">category</label>
                        <select name="category_id[]" class=" form-control   selectpicker" multiple="multiple" data-live-search="true" required>
                            @if(isset($categories) && $categories -> count() > 0)
                            @foreach($categories as $category)


                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                            @endif
                        </select>

                        @error('category')
                        <small class="'form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>


                    <div class="form-group">
                        <label for="formGroupExampleInput">product number</label>
                        <input type="numbers" name="productnumber"  id="productnumber" placeholder="productnumber">
                        @error('productnumber')
                        <small class="'form-text text-danger">{{$message}}</small>
                        @enderror
                        <br>


                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">description</label>
                        <textarea  name="description" id="exampleFormControlTextarea1" rows="3"></textarea>
                        @error('description')
                        <small class="'form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>




                    <div class="form-group">
                        <label for="formGroupExampleInput">Price</label>
                        <input type="numbers" name="price" id="price" placeholder="price">
                        @error('price')
                        <small class="'form-text text-danger">{{$message}}</small>
                        @enderror
                        <br>
                        <div>
                            <button type="submit" class="btn btn-primary p-2 m-2">Submit</button>
                        </div>

                    </div>

            </form>
        </div>

        <div class="col-sm">

        </div>
    </div>

</div>


<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script>
    $(document).ready(function() {
        // Select2 Multiple
        $('.select2-multiple').select2({
            placeholder: "Select",
            allowClear: true
        });

    });

</script>
@endsection


