@extends('admin.layout.app')
@section('content')







<div class="container">


    <div class=" dev1 ">

        <h1 class="text-6xl font-bold pt-10">
        {{$product->name}}
        </h1>

        <span>

        <div> last update on: <span class="text-gray-500 italic"> {{date('m-d-Y'), strtotime($product->updated_at)}}</span></div>
    </div>
<div class=" dev1 m-2 ">
    <p> has relation with these categories:</p>

     @foreach ($product->categories as $category )
    <span></span><h6>{{ $category->name }}.</h6></span>
@endforeach</div>


<div class="container m-auto pt-15 pb-5">

<div class="dev1">

    <img  src="/images/products/{{ $product->photo}}" class="img-thumbnail image1 dev1 center" alt="Max-width 40% Max-high 40%" height: 600px>
</div >

<div class="div1 m-2 b-2">
<span class="dev1">
    {{$product->description}}
</span>
</div>



<br><br>



<div class="center dev1">
    <a href="{{ Route('admin.makerelated',$product->id) }}" class="btn btn-success p-2  m-3"><h6>add and edit relation</h6></a>

    <a href="{{ Route('admin.editproduct', $product->id) }}" class="btn btn-primary p-2 m-3 "><h6>edite</h6></a>

    <a href="{{ Route('admin.deleteproduct', $product->id) }}" class="btn btn-danger p-2 m-3  "><h6>delete</h6></a>

</div>



</div>
</div>
@endsection


