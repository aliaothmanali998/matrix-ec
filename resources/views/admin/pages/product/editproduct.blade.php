
@extends('admin.layout.app')
@section('content')







<div class="container">

    <div class="row x">

        <div class=" col-12">
            <br>
            <br>
            <br>
            <form  action="{{ route('admin.updateproduct',$product->id) }}" method="POST" enctype="multipart/form-data">
                @csrf


                <div class="form-group">
                    <label for="formGroupExampleInput">name of product</label>
                    <input type="text" name="name"  id="name-of-product" placeholder="name of product" value="{{$product->name}}">
                    @error('name')
                    <small class="'form-text text-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="formGroupExampleInput">   <img class="object-cover" src="/images/products/{{ $product -> photo}}" alt="" style="width: 100px"> </label>
                    <input type="file" name="photo"  id="Offer Name" placeholder="photo of product" value="  ">
                    @error('photo')
                    <small class="'form-text text-danger">{{$message}}</small>
                    @enderror



                    <div class="form-group">
                        <label for="formGroupExampleInput">Product number</label>
                        <input type="numbers" name="productnumber"  id="productnumber" placeholder="productnumber" value="{{$product->productnumber}}">
                        @error('productnumber')
                        <small class="'form-text text-danger">{{$message}}</small>
                        @enderror
                        <br>


                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">description</label>
                        <textarea  name="description" id="exampleFormControlTextarea1" rows="3">{{$product->description}}</textarea>
                        @error('description')
                        <small class="'form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>




                    <div class="form-group">
                        <label for="formGroupExampleInput">price</label>
                        <input type="numbers" name="price" id="price" placeholder="price" value="{{$product->price}}">
                        @error('price')
                        <small class="'form-text text-danger">{{$message}}</small>
                        @enderror
                        <br>
                        <div>
                            <button type="submit" class="btn btn-primary p-2 m-2">Submit</button>
                        </div>

                    </div>

            </form>
        </div>

        <div class="col-sm">

        </div>
    </div>

</div>


<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script>
    $(document).ready(function() {
        // Select2 Multiple
        $('.select2-multiple').select2({
            placeholder: "Select",
            allowClear: true
        });

    });

</script>
@endsection


