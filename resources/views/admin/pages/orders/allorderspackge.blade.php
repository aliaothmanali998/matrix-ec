@extends('admin.layout.app')
@section('content')


<div>
    <h3 class="text-center"> All Orders</h3>
</div>


    <table class="table" id="mytable">
        <thead>
        <tr>
            <th scope="col">user name</th>
            <th scope="col">email</th>
            <th scope="col">phone</th>
            <th scope="col">address</th>
{{--            <th scope="col">Product</th>--}}
{{--            <th scope="col">Quantity</th>--}}
            <th scope="col">Date</th>
            <th scope="col">price</th>
            <th scope="col">delivery status</th>






        </tr>
        </thead>
        <tbody>

        @foreach($orders as $order)
            <tr>


                <th scope="col">{{ $order->user->name }}</th>
                <td scope="col">{{$order ->user->email}}</td>
                <td scope="col">{{$order ->user->phone}}</td>
                <td scope="col">{{$order ->user->address}}</td>
{{--                <td> {{$order->product->name}} </td>--}}
{{--                <td scope="col">{{$order ->quantity}}</td>--}}
                <td scope="col">{{$order ->updated_at}}</td>
                <td scope="col">{{$order ->price}} $</td>
                <td scope="col">
                    @if($order->delivery_status=='processing')
                        {{$order->delivery_status}}
                        <a href="" class="btn btn-primary m-1">Delivered</a>
{{--                        {{route('admin.deliveredone',$order->id)}}                       onclick="return confirm('Are you sure this product is delivered')" --}}
                    @else
                        <p>Delivered</p>
                    @endif

                </td>





            </tr>
        @endforeach
        </tbody>


    </table>
@endsection
