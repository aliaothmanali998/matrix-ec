@extends('admin.layout.app')
@section('content')


<div>
    <h3 class="text-center"> All Orders</h3>
</div>


    <table class="table" id="mytable">
        <thead>
        <tr>
            <th scope="col">user name</th>
            <th scope="col">email</th>
            <th scope="col">phone</th>
            <th scope="col">address</th>

{{--            <th scope="col">delivery status</th>--}}
            <th scope="col">operation</th>




        </tr>
        </thead>
        <tbody>

        @foreach($uniqueCollection as $order)
            <tr>


                <th scope="col">{{ $order->user->name }}</th>
                <td scope="col">{{$order ->user->email}}</td>
                <td scope="col">{{$order ->user->phone}}</td>
                <td scope="col">{{$order ->user->address}}</td>


{{--                <td scope="col">--}}
{{--                    @if($order->delivery_status=='processing')--}}
{{--                        {{$order->delivery_status}}--}}
{{--                        <a href="{{route('admin.delivered',$order->created_at)}}" onclick="return confirm('Are you sure this product is delivered')" class="btn btn-primary m-1">Delivered</a>--}}
{{--                    @else--}}
{{--                        <p>Delivered</p>--}}
{{--                    @endif--}}

{{--                </td>--}}




                <td scope="col"><a href="{{route('admin.showorderdetails', ['id'=>"$order->user_id"])}}" class="btn btn-success">show order</a></td>



            </tr>
        @endforeach
        </tbody>


    </table>
@endsection
