@extends('admin.layout.app')
@section('content')


@if(Session::has('success'))
<div class="alert alert-success" role="alert">

        <span aria-hidden="true">&times;</span>
      </button>
    <h5>{{Session::get('success')}}</h5>
</div>
@endif
@if(Session::has('deletemessage'))
<div class="alert alert-danger" role="alert">

  <h5> {{Session::get('deletemessage')}}</h5>
</div>
@endif
@if(Session::has('successp'))
<div class="alert alert-success" role="alert">

        <span aria-hidden="true">&times;</span>

    <h5>{{Session::get('successp')}}</h5>
</div>
@endif

<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">








        <div class="container-fluid py-4">


            <div class="helen">
                <a href="{{ Route('admin.createuser') }}" class="btn btn-success p-2  ">create</a>
            </div>


            <table class="table" id="mytable">
                <thead>
                <tr>

                    <th scope="col">user name</th>
                    <th scope="col">email</th>
                    <th scope="col">phone</th>
                    <th scope="col">address</th>
                    <th scope="col">operation</th>



                </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        @if($user-> user_type == 0)

                        <tr>
                            <td scope="col">
                               {{ $user -> name }}
                                </td>

                            <td scope="col">{{$user -> email}}</td>
                            <td scope="col">{{$user -> phone}}</td>
                            <td scope="col">{{$user -> address}}</td>
                            <td> <div >


                                    <a href="{{ Route('admin.edituser', $user->id) }}" class="btn btn-primary p-2  ">edite</a>

                                    <a href="{{ Route('admin.deleteuser', $user->id) }}" class="btn btn-danger p-2  ">delete</a>

                                    <a href="{{ Route('admin.showuser', $user->id) }}" class="btn btn-warning p-2  ">show details</a>
                            </div>
                        </td>
                        </tr>
                        @endif
                            @endforeach

                        </tbody>


            </table>

    </main>










@endsection

