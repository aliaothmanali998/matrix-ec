@extends('admin.layout.app')
@section('content')

@if(Session::has('success'))
<div class="alert alert-success" role="alert">

    <h5>{{Session::get('success')}}</h5>
</div>
@endif
@if(Session::has('deletemessage'))
<div class="alert alert-danger" role="alert">
  <h5> {{Session::get('deletemessage')}}</h5>
</div>
@endif
@if(Session::has('successp'))
<div class="alert alert-success" role="alert">
    <h5>{{Session::get('successp')}}</h5>
</div>
@endif

    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
        <!-- Navbar -->


        <!-- End Navbar -->


        <div class="helen">
            <a href="{{ Route('admin.createcategory') }}" class="btn btn-success p-2  ">create</a>
        </div>


        @if(isset($categories) && $categories -> count() > 0)
        @foreach($categories as $category)
            <h5>{{$category->name}}</h5>
            <div class="helen">

        <a href="{{ route('admin.showproduct',$category->id) }}" class="btn btn-primary p-2">show related product</a>
        <a href="{{ route('admin.editcategory',$category->id) }}" class="btn btn-success p-2">edit category name</a>
        <a href="{{ route('admin.deletecategory',$category->id) }}" class="btn btn-danger p-2">delete category </a>
            </div>
{{--     <p> {{ $category->product->name}}</p>--}}
        @endforeach
        @endif





@endsection
