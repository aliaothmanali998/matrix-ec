@extends('admin.layout.app')
@section('content')



    @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            <h5>{{Session::get('success')}}</h5>
        </div>
    @endif
    @if(Session::has('message'))
        <div class="alert alert-success" role="alert">
            <h5>{{Session::get('message')}}</h5>
        </div>
    @endif

  <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    <!-- Navbar -->


    <div class="container-fluid py-4">


        <div class="helen">
            <a href="{{ Route('admin.createinv') }}" class="btn btn-success p-2  ">create</a>
        </div>


        <table class="table" id="mytable">
            <thead>
            <tr>

                <th scope="col">Product</th>
                <th scope="col">Quantity</th>
                <th scope="col">Date</th>

                <th scope="col">Type</th>
                <th scope="col">Operation</th>



            </tr>
            </thead>
            <tbody>

                    @foreach($inventories as $inventory)
                    <tr>


                        <th scope="col">{{ $inventory->product->name }}</th>
                        <td scope="col">{{$inventory ->qty}}</td>
                        <td scope="col">{{$inventory ->updated_at}}</td>

                        <td scope="col">@if($inventory->type==0)
                                         <p>add</p>
                                        @else
                                        <p>subtract</p>
                                        @endif
                        </td>

                        <td> <div >


                                <a href="{{ Route('admin.editinv', $inventory->id) }}" class="btn btn-primary p-2  ">edite</a>




                        </div>
                    </td>
                    </tr>
                        @endforeach
                    </tbody>


        </table>

@endsection
