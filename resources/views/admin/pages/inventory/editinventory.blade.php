@extends('admin.layout.app')
@section('content')

    @if(Session::has('message'))
        <div class="alert alert-success" role="alert">
            <h5>{{Session::get('message')}}</h5>
        </div>
    @endif
    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">





        <div class="container">

            <div class="row x">

                <div class=" col-12">
                    <br>
                    <br>
                    <br>
                    <form  action="{{ route('admin.updateinv',$inventory->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group">
                            <label for="formGroupExampleInput2">product </label>
                            <select name="product_id" class=" form-control   selectpicker" value="chose one"  data-live-search="true" required>


                                        <option value="{{ $inventory->product->id }}"><strong>{{ $inventory->product->name }}</strong>        (and you have {{$inventory->product->quantity}})</option>



                            </select>

                            @error('name')
                            <small class="'form-text text-danger">{{$message}}</small>
                            @enderror
                        </div>


                        <div class="form-group">
                            <label for="formGroupExampleInput">type of operation</label>
                            <select name="type"  class=" form-control   selectpicker" multiple="multiple" data-live-search="true" >



                                <option value="0">add</option>
                                <option value="1">subtract</option>


                            </select>

                            @error('type')
                            <small class="'form-text text-danger">{{$message}}</small>
                            @enderror

                            <label for="formGroupExampleInput">quantity</label>

                            <input type="text" name="qty"  id="Offer Name" placeholder="quantity" value="{{$inventory->quantity}}">

                            @error('qty')
                            <small class="'form-text text-danger">{{$message}}</small>
                            @enderror

                            <button type="submit" class="btn btn-primary p-2 m-2">Submit</button>
                        </div>

                </div>

                </form>
            </div>

            <div class="col-sm">

            </div>
        </div>

        </div>





@endsection
