@extends('admin.layout.app')
@section('content')




    <div class="container-fluid py-4"








    <div class="row">
        <div class="col-lg-7 position-relative z-index-2">
            <div class="card card-plain mb-4">
                <div class="card-body p-3">
                    <div class="row">
                        <div class="col-lg-6">


                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-5 col-sm-5">
                    <div class="card  mb-2">
                        <div class="card-header p-3 pt-2">
                            <div class="icon icon-lg icon-shape bg-gradient-dark shadow-dark shadow text-center border-radius-xl mt-n4 position-absolute">
                                <i class="material-icons opacity-10">weekend</i>
                            </div>
                            <div class="text-end pt-1">
                                <p class="text-sm mb-0 text-capitalize "><h4>products</h4></p>
                                <h5 class="mt-3 mb-2 ms-3 mt-2 ">the quantity of our products is {{$productq}}</h5>
                                <h5 class="mt-3 mb-2 ms-3 ">the number of our products is {{$productn}}</h5>
                            </div>
                        </div>

                        <hr class="dark horizontal my-0">

                    </div>



                </div>


                <hr class="horizontal my-0 dark">

            </div>



        </div>
    </div>

















    <div class="row mt-4">
        <div class="col-10">
            <div class="card mb-4 ">
                <div class="d-flex">

                    <h2 class="mt-3 mb-2 ms-3 ">the orders of today</h2>
                </div>
                <div class="card-body p-3">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="table-responsive">

                                <div class=" pt-1 m-2">


                                    <table class="table" id="mytable">
                                        <thead>
                                        <tr>
                                            <th scope="col">user name</th>
                                            <th scope="col">email</th>

                                            <th scope="col">Product</th>
                                            <th scope="col">Quantity</th>
                                            <th scope="col">Date</th>
                                            <th scope="col">price</th>
                                            <th scope="col">Delivery status</th>
                                            <th scope="col">Delevered</th>





                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($orders as $order)
                                            <tr>


                                                <th scope="col">{{ $order->user->name }}</th>
                                                <td scope="col">{{$order ->user->email}}</td>
                                                <td scope="col">{{$order ->product->name}}</td>
                                                <td scope="col">{{$order ->quantity}}</td>

                                                <td scope="col">{{$order ->updated_at}}</td>
                                                <td scope="col">{{$order ->price}} $</td>
                                                <td scope="col">{{$order ->delivery_status}} </td>


                                                <td scope="col"><a href="{{route('admin.showorderdetails', $order->created_at)}}" class="btn btn-success">show order</a> </td>




                                            </tr>
                                        @endforeach
                                        </tbody>


                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>



















    <div class="col-lg-8 col-sm-8 mt-sm 0 mt-5 p-2">
        <div class="card  mb-2">
            <div class="card-header p-3 p-2 bg-transparent">
                <div class="icon icon-lg icon-shape bg-gradient-success shadow-success text-center border-radius-xl mt-n4 position-absolute">
                    <i class="material-icons opacity-10 ">store</i>
                </div>
                <div class="text-end p-4">
                    <p class="text-sm mb-0 text-capitalize border-2 ">today's user</p>
                    <div class="p-4">

                        <table class="table m-4 p-4 " id="myusertable">
                            <thead>
                            <tr>

                                <th scope="col">user name</th>
                                <th scope="col">email</th>
                                <th scope="col">Date</th>





                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                @if($user-> user_type == 0)

                                    <tr>
                                        <td scope="col">
                                            {{ $user -> name }}
                                        </td>

                                        <td scope="col">{{$user -> email}}</td>
                                        <td scope="col">{{$user ->updated_at}}</td>


                                    </tr>
                                @endif
                            @endforeach

                            </tbody>


                        </table>

                    </div>
                </div>


            </div>



        </div>
    </div>







    <div class="row mt-4">
        <div class="col-10">
            <div class="card mb-4 ">
                <div class="d-flex">

                    <h2 class="mt-3 mb-2 ms-3 ">the Inventories of today</h2>
                </div>
                <div class="card-body p-3">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="table-responsive">

                                <div class=" pt-1 m-2">


                                    <table class="table" id="myusertable">
                                        <thead>
                                        <tr>

                                            <th scope="col">Product</th>
                                            <th scope="col">Quantity</th>
                                            <th scope="col">Date</th>
                                            <th scope="col">Type</th>


                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($inventories as $inventory)
                                            <tr>


                                                <th scope="col">{{ $inventory->product->name }}</th>
                                                <td scope="col">{{$inventory ->qty}}</td>
                                                <td scope="col">{{$inventory ->updated_at}}</td>

                                                <td scope="col">@if($inventory->type==0)
                                                        <p>add</p>
                                                    @else
                                                        <p>subtract</p>
                                                    @endif
                                                </td>


                                            </tr>
                                        @endforeach
                                        </tbody>


                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>



    </main>




@endsection
